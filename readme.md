# ISSUE tracking:

## API + FRONTEND
I've decided to separate frontEnd and backEnd, that's why You can find two completely independent directories:

- frontend
- backedn

both have separate readme.md, package.json, etc.

For testing You need to build in both of them:
in backend:
```$xslt
npm run dev
```

in frontend:
```$xslt
npm run dev || npm run build && http-server -p 8082
```

and Finally You can test both on:

[LINK - click me](http://localhost:8082/dist/))
