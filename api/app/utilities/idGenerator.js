const Issues = require('../dbCollections/IssueCollection');

const getCurrentId = () => new Promise((resolve, reject) => {
    Issues.findOne({}, { sort: { id: -1 } }, (err, issue) => {
        if (err) {
            return reject(err);
        } else if (issue && Number.isInteger(issue.id)) {
            return resolve(issue.id);
        }
        return 0;
    });
});
let currentId = 0;

getCurrentId()
    .then((id) => {
        currentId = id;
    })
    .catch(err => console.log(err.message));

function* idGenerator() {
    while (true) {
        // eslint-disable-next-line no-plusplus
        yield ++currentId;
    }
}

module.exports = idGenerator();
