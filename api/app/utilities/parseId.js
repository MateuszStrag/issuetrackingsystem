/* eslint-disable no-restricted-globals */
module.exports = (req, res, next) => {
    req.params.id = parseInt(req.params.id, 10);
    if (isNaN(req.params.id)) {
        next({ message: 'wrong type of id, please provide integer' });
    } else {
        next();
    }
};
