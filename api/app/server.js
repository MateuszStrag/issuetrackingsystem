const app = require('./IssueApp');

const PORT = process.env.PORT || 8081;

if (!module.parent) {
    app.listen(PORT);
}
console.log(`Started on port: ${PORT}`);

module.exports = app;
