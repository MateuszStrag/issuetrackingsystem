const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./routes/apiRoute');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
/**
 * Handle request for : /api
 */
app.use('/api', api);
/**
 * below method catch errors which was forwarded to next()
 */
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    console.log('error: ', err.message);
    res.status(500).json({ status: 500, message: 'something went wrong' });
});
/**
 * Handle request without any handlers (404)
 */
// eslint-disable-next-line no-unused-vars
app.use((req, res, next) => {
    console.log('404 NOT FOUND');
    res.status(404).json({ status: 404, message: 'Not found' });
});

module.exports = app;
