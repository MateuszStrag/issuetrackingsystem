const Issues = require('../dbCollections/IssueCollection');
const idGenerator = require('../utilities/idGenerator');

function getIssue(id) {
    return new Promise((resolve, reject) => {
        Issues.findOne({ id }, (err, item) => {
            if (err) {
                return reject(err);
            }
            return resolve(item);
        });
    });
}

function getAllIssues() {
    return new Promise((resolve, reject) => {
        Issues.find({}).toArray((err, items) => {
            if (err) {
                return reject(err);
            }
            return resolve(items);
        });
    });
}

function save(requestIssue) {
    const newId = idGenerator.next().value;
    const issue = Object.assign({}, requestIssue, { id: newId });
    return new Promise((resolve, reject) => {
        Issues.insert(issue, (err, item) => {
            if (err) {
                return reject(err);
            }
            return resolve(item[0]);
        });
    });
}

function update(issue) {
    return new Promise((resolve, reject) => {
        Issues.findAndModify({ id: issue.id }, [['id', 1]], issue, { new: true, upsert: true, w: 1 }, (err) => {
            if (err) {
                return reject(err);
            }
            return resolve();
        });
    });
}
module.exports = {
    getIssue,
    getAllIssues,
    save,
    update,
};
