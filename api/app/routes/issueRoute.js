const express = require('express');
const Issue = require('../models/IssueModel');
const parseId = require('../utilities/parseId');

const router = express.Router();
/**
 *  GET fetch issue from DB, request based on id provided, middleware for parsing string into INT
 */
router.route('/:id')
    .get(parseId, (req, res, next) => {
        Issue.getIssue(req.params.id)
            .then((issue) => {
                if (issue != null) {
                    res.status(200).json(issue);
                } else {
                    res.status(404).json({ message: 'no issue found' });
                }
            })
            .catch(next);
    });

router.route('/')
    .get((req, res, next) => {
        Issue.getAllIssues()
            .then(issues => res.status(200).json(issues))
            .catch(next);
    })
    .post((req, res, next) => {
        const issue = {
            title: req.body.title,
            description: req.body.description,
            status: req.body.status,
        };
        Issue.save(issue)
            .then(createdIssue => res.status(201).json({ message: 'Issue created', createdIssue }))
            .catch(next);
    })
    .put((req, res, next) => {
        const issue = {
            id: parseInt(req.body.id, 10),
            title: req.body.title,
            description: req.body.description,
            status: req.body.status,
        };
        Issue.update(issue)
            .then(() => res.status(201).json({ message: 'Issue updated' }))
            .catch(next);
    });

module.exports = router;
