const express = require('express');
const issue = require('./issueRoute');

const router = express.Router();

router.route('/health-check')
    .get((req, res) => {
        res.json({
            status: 'ok',
        });
    });
router.use('/issues', issue);

module.exports = router;
