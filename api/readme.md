## Installation API project:
ensure that You are in proper directory (api)
```
npm install || npm i
```

## run dev server (do not use in production):
```
npm run dev
```
default port is set to 8081

## Database:
Currently I used tingoDB database:
it write all data into single file 'issues' - which is localized in api/db

You are starting with empty database.

If You decide to start again with empty DB, please removev this file: api/db/issues 

## API:
Endpoints:

GET api/issues - fetch All issues
GET api/issues/2 - fetch issue with id === 2
POST api/issues - save whole request payload (without validation) 

## ESlint:

```$xslt
npm run eslint
```

## tests:

```$xslt
npm run test
```


## TODO:

- migrate to stable, scalable database 
- make validation of user input
- create configuration endpoint (including restrictions: maxLength, etc.)
- remove unnecessary headers which expose using express.js
- use coverage report

