const request = require('supertest-as-promised');
const chai = require('chai');
const app = require('../app/server');
const { expect } = chai;

describe('### Misc', () => {
    describe('# GET /api/health-check', () => {
        it('should return OK', (done) => {
            request(app)
                .get('/api/health-check')
                .expect(200)
                .then((res) => {
                    expect(res.body.status).to.equal('ok');
                    done();
                })
                .catch(done);
        });
    });

    describe('# GET /api/404', () => {
        it('should return 404 status', (done) => {
            request(app)
                .get('/api/404')
                .expect(404)
                .then((res) => {
                    expect(res.body.message).to.equal('Not found');
                    done();
                })
                .catch(done);
        });
    });
});