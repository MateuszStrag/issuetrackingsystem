const sinon = require('sinon');
const Issues = require('../app/dbCollections/IssueCollection');
const IssueModel = require('../app/models/IssueModel');
const idGenerator = require('../app/utilities/idGenerator');

describe('### Issue Component', () => {
    const sandbox = sinon.sandbox.create();
    const sampleId = 999;
    beforeEach(function() {
        sandbox.restore();
        sandbox.stub(Issues, 'findOne');
        sandbox.stub(Issues, 'find');
        sandbox.stub(Issues, 'insert');
        sandbox.stub(idGenerator, 'next').callsFake(() => ({ value: sampleId }));
    });
    afterEach(function() {
        sandbox.restore();
    });
    it(' save method should called collection.insert with proper values', (done) => {
        const sampleIssue = {
            title: 'some title',
            description: 'some description',
            status: 'some status',
        };
        IssueModel.save(sampleIssue);
        sinon.assert.calledOnce(Issues.insert);
        sinon.assert.calledOnce(idGenerator.next);
        sinon.assert.calledWithMatch(Issues.insert, Object.assign(sampleIssue, { id: sampleId }));
        done();
    });

    it('getIssue method should called collection.findOne with proper id', (done) => {
        IssueModel.getIssue(sampleId);
        sinon.assert.calledOnce(Issues.findOne);
        sinon.assert.calledWithMatch(Issues.findOne, { id: sampleId });
        done();
    });

    it('getAllIssues method should called collection.find', (done) => {
        IssueModel.getAllIssues().catch(() => {});
        sinon.assert.calledOnce(Issues.find);
        done();
    });
});
