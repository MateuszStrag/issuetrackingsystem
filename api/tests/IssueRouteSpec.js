const request = require('supertest-as-promised');
const chai = require('chai');
const sinon = require('sinon');
const Issues = require('../app/dbCollections/IssueCollection');
const IssueModel = require('../app/models/IssueModel');
const app = require('../app/server');

const { expect } = chai;

describe('### ISSUES ROUTE', () => {

    describe('# Error Handling', () => {

        it('should return error when wrong id is provided', (done) => {
            request(app)
                .get('/api/issues/i18n')
                .expect(500)
                .then((res) => {
                    expect(res.body.message).to.equal('something went wrong');
                    done();
                })
                .catch(done);
        });
    });

    describe('# Should fire IssueModel method:', () => {
        const sandbox = sinon.sandbox.create();
        const sampleId = 999;
        const sampleIssue = {
            title: 'some title',
            description: 'some description',
            status: 'some status',
        };
        const returnedIssue = Object.assign({}, sampleIssue, { id: sampleId });
        beforeEach(function() {
            sandbox.restore();
            sandbox.stub(IssueModel, 'save').callsFake((issue)=> {
                return new Promise((resolve, reject) => resolve(Object.assign({}, issue, { id: sampleId })));
            });
            sandbox.stub(IssueModel, 'getIssue').callsFake((id)=> {
                return new Promise((resolve, reject) => resolve(Object.assign({}, sampleIssue, { id })));
            });
            sandbox.stub(IssueModel, 'getAllIssues').callsFake(()=> {
                return new Promise((resolve, reject) => resolve([sampleIssue, sampleIssue]));
            });
        });
        afterEach(function() {
            sandbox.restore();
        });

        it('save', (done) => {
            request(app)
                .post('/api/issues')
                .send(sampleIssue)
                .expect(201)
                .then((res)=> {
                    sinon.assert.calledOnce(IssueModel.save);
                    sinon.assert.calledWithExactly(IssueModel.save, sampleIssue);
                    expect(res.body.message).to.equal('Issue created');
                    expect(res.body.createdIssue).to.deep.equal(returnedIssue);
                    done();
                });
        });

        it('getIssue', (done) => {
            request(app)
                .get(`/api/issues/${sampleId}`)
                .expect(200)
                .then((res)=> {
                    sinon.assert.calledOnce(IssueModel.getIssue);
                    sinon.assert.calledWithExactly(IssueModel.getIssue, sampleId);
                    expect(res.body).to.deep.equal(returnedIssue);
                    done();
                });
        });

        it('getAllIssues', (done) => {
            request(app)
                .get('/api/issues')
                .expect(200)
                .then((res)=> {
                    sinon.assert.calledOnce(IssueModel.getAllIssues);
                    expect(res.body).to.deep.equal([sampleIssue, sampleIssue]);
                    done();
                });
        });
    });
});