const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

config = {
    entry: {
        "issue-tracking-system": "./app/index.jsx",
        "issue-tracking-system.min": "./app/index.jsx"
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)/,
                exclude: [/node_modules/],
                loader: 'babel-loader',
                query: {
                    presets: ['env', 'react', 'react-hmre']
                }
            }
        ]
    },
    plugins: [
        new UglifyJsPlugin({
            include: /\.min\.js$/
        }),
    ],
    devtool: process.env.NODE_ENV === 'development' ? 'inline-source-map' : false,
    devServer: {
        port: 8082,
        disableHostCheck: true,
        hot: true
    },
};

if (process.env.NODE_ENV === 'development') {
    config.plugins.push(
        new HtmlWebpackPlugin({
            template: 'example/index.html',
            excludeChunks: ["issue-tracking-system.min"]
        }));
} else {
    config.plugins.push(
        new HtmlWebpackPlugin({
            template: 'example/index.html',
            excludeChunks: ["issue-tracking-system"]
        }));

}

module.exports = config;