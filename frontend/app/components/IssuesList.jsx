import React from 'react';
import PropTypes from 'prop-types';
import Issue from './Issue.jsx';
import { NEW, OPEN, CLOSED } from '../constants/StatusTypes';

const IssueList = ({
    issues, onIssueClick,
}) => (
    <ul
        className="list-group"
    >
        {issues.map(issue => (
            <Issue key={issue.id} {...issue} onClick={() => onIssueClick(issue)} />
        ))}
    </ul>
);

IssueList.propTypes = {
    issues: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        status: PropTypes.oneOf([NEW, OPEN, CLOSED]),
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
    }).isRequired).isRequired,
    onIssueClick: PropTypes.func.isRequired,
};

export default IssueList;
