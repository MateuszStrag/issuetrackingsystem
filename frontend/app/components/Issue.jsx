import React from 'react';
import PropTypes from 'prop-types';
import { NEW, OPEN, CLOSED } from '../constants/StatusTypes';

const Issue = ({
    id, status, title, description, onClick,
}) => (
    <li className="list-group-item" key={id}>
        <h3>{title} <span className="badge badge-secondary">{status}</span></h3>
        <h6>{description}</h6>
        <button
            className="btn btn-primary"
            onClick={onClick}
        >
            Change Status
        </button>
    </li>
);

Issue.propTypes = {
    id: PropTypes.number.isRequired,
    status: PropTypes.oneOf([NEW, OPEN, CLOSED]).isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

export default Issue;
