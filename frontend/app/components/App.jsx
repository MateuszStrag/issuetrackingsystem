import React from 'react';
import AddIssue from '../containers/AddIssue.jsx';
import IssuesList from '../containers/IssuesList';

const App = () => (
    <div className="container">
        <AddIssue />
        <IssuesList />
    </div>
);

export default App;
