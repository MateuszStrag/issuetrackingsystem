import { connect } from 'react-redux';
import IssuesList from '../components/IssuesList.jsx';
import { upgradeStatus } from '../actions/index';

const mapStateToProps = state => ({
    issues: state.issues,
});

const mapDispatchToProps = dispatch => ({
    onIssueClick: (issue) => {
        dispatch(upgradeStatus(issue));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(IssuesList);
