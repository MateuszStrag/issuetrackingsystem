import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addIssue } from '../actions';

const AddIssue = ({ dispatch }) => {
    let title;
    let description;

    return (
        <div>
            <form
                onSubmit={(e) => {
                    e.preventDefault();
                    if (!title.value.trim()) {
                        return;
                    }
                    dispatch(addIssue({ title: title.value, description: description.value.trim() }));
                    title.value = '';
                    description.value = '';
                }}
            >
                <div className="form-group">
                    <input
                        ref={(node) => {
                            title = node;
                        }}
                        className="form-control"
                        placeholder="Title of task..."
                    />
                </div>

                <div className="form-group">
                    <input
                        ref={(node) => {
                            description = node;
                        }}
                        className="form-control"
                        placeholder="Description of task..."
                    />
                </div>
                <button className="btn btn-primary btn-block" type="submit">
                    Add Issue
                </button>
            </form>
        </div>
    );
};

AddIssue.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

export default connect()(AddIssue);
