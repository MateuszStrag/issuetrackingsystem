import {
    GET_ALL_ISSUES,
    ADD_ISSUE, GET_ALL_ISSUES_RECEIVED,
    ADD_ISSUE_RECEIVED, UPGRADE_STATUS,
    UPGRADE_STATUS_RECEIVED,
} from '../constants/ActionTypes';
import { CLOSED, NEW, OPEN } from '../constants/StatusTypes';
import store from '../store/configureStore';
import { getAllIssues, saveNewIssue, updateIssue } from '../services/apiService';

export function upgradeStatus(status) {
    switch (status) {
    case NEW:
        return OPEN;
    case OPEN:
        return CLOSED;
    default:
        return status;
    }
}

const apiService = async (state = [], action) => {
    switch (action.type) {
    case GET_ALL_ISSUES: {
        const allIssues = await getAllIssues();

        store.dispatch({ type: GET_ALL_ISSUES_RECEIVED, issues: allIssues });
        return [
            ...state,
            ...allIssues,
        ];
    }

    case ADD_ISSUE: {
        const newIssue = {
            title: action.title,
            description: action.description,
            status: NEW,
        };
        const data = await saveNewIssue(newIssue);
        store.dispatch({ type: ADD_ISSUE_RECEIVED, issue: data.createdIssue });
        return [
            ...state,
            data.createdIssue,
        ];
    }
    case UPGRADE_STATUS: {
        const updatedIssue = Object.assign({}, action, { status: upgradeStatus(action.status) });
        await updateIssue(updatedIssue);
        store.dispatch({ type: UPGRADE_STATUS_RECEIVED, issue: updatedIssue });
        return updatedIssue;
    }
    default:
        return state;
    }
};

export default apiService;
