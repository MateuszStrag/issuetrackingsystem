import { UPGRADE_STATUS_RECEIVED, GET_ALL_ISSUES_RECEIVED, ADD_ISSUE_RECEIVED } from '../constants/ActionTypes';

const issues = (state = [], action) => {
    switch (action.type) {
    case ADD_ISSUE_RECEIVED:
        return [
            ...state,
            action.issue,
        ];
    case UPGRADE_STATUS_RECEIVED:
        return state.map((issue) => {
            if (issue.id === action.issue.id) {
                return Object.assign({}, issue, { status: action.issue.status });
            }

            return issue;
        });
    case GET_ALL_ISSUES_RECEIVED:
        return [
            ...state,
            ...action.issues,
        ];
    default:
        return state;
    }
};

export default issues;
