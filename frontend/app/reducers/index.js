import { combineReducers } from 'redux';
import api from './api';
import issues from './issues';

const rootReducer = combineReducers({
    api,
    issues,
});

export default rootReducer;
