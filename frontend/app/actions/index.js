import { ADD_ISSUE, UPGRADE_STATUS } from '../constants/ActionTypes';

export const addIssue = issue => Object.assign({}, issue, { type: ADD_ISSUE });
export const upgradeStatus = issue => Object.assign({}, issue, { type: UPGRADE_STATUS });
