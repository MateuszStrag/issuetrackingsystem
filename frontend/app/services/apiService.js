import 'babel-polyfill';
import 'isomorphic-fetch';

const API_URL = 'http://localhost:8081/api/issues/';

export function saveNewIssue(issue) {
    const options = {
        method: 'POST',
        body: JSON.stringify(issue),
        headers: {
            'content-type': 'application/json',
        },
    };
    return fetch(API_URL, options)
        .then(resp => resp.json())
        .catch(console.log);
}

export function updateIssue(issue) {
    const options = {
        method: 'PUT',
        body: JSON.stringify(issue),
        headers: {
            'content-type': 'application/json',
        },
    };
    return fetch(API_URL, options)
        .then(resp => resp.json())
        .catch(console.log);
}

export function getAllIssues() {
    return fetch(API_URL)
        .then(resp => resp.json())
        .catch(console.log);
}
