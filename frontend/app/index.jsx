import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './components/App.jsx';
import store from './store/configureStore';
import { GET_ALL_ISSUES } from './constants/ActionTypes';


render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('IssueTrackingSystem'),
);
store.dispatch({ type: GET_ALL_ISSUES });
