## Installation FRONTEDN project:
ensure that You are in proper directory (frontend)
```
npm install || npm i
```

## run dev server :
```
npm run dev
```
default port is set to 8082 (require backend run)

after build (dev) You can visit:
[LINK - click me](http://localhost:8082/dist)

## Building: :
```
npm run build
```


## ESlint:

```$xslt
npm run eslint
```

## tests:

```$xslt
npm run test
```

NOTES:
Due to lack of time, I didn't wrote any tests, or didn't import sass/less :( (but I got it in my backlog :))

